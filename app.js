var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
const geojsonRouter = require('./routes/geojson');
var app = express();

var server = require('http').Server(app);
var io = require('socket.io')(server);

app.use(function(req, res, next){
  res.io = io;
  next();
});

app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit:50000 }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/geojson', geojsonRouter);

module.exports = {app: app, server: server};
