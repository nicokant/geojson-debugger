var rp = require('request-promise');

async function debugGeoJSON(geojson, options) {
  const body = {
    geojson,
    group: options.group || 'Undefined',
    time: new Date().getTime(),
    id: options.id,
  };

  return await rp({
    method: 'POST',
    uri: 'http://localhost:3000/geojson',
    body,
    json: true
  })
}

module.exports = debugGeoJSON;
