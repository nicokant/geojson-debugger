var express = require('express');
var router = express.Router();

/* POST a geojson layer */
router.post('/', function(req, res, next) {
  res.io.emit('add-geojson', req.body);
  res.send({ success: true });
});

module.exports = router;
