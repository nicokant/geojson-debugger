var mymap = L.map('mapid')
  .setView([45.6654, 9.3727], 10);

const osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
  maxZoom: 18,
  id: 'osm'
}).addTo(mymap);

const control = L.control.layers({
  "OSM": osm,
}, {}, { collapsed: false }).addTo(mymap);

const socket = io();
socket.on('add-geojson', geojsonData => {
  var myLayer = L.geoJSON().addTo(mymap);
  myLayer.addData(geojsonData.geojson);

  const name = geojsonData.id || geojsonData.group + geojsonData.time;
  control.addOverlay(myLayer, name);

  setTimeout(() =>  {
    const spans = document.evaluate(`//span[contains(., '${geojsonData.time}')]`, document, null, XPathResult.ANY_TYPE, null );
    const input = spans.iterateNext().previousElementSibling;
    input.click();
    }, 10)
});
